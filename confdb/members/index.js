var { Members } = require('../db.js');

var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

module.exports = {
    loginMember: function(args, callback) {
        Members.findOne({ username: args.username }, function (err, result) {
          if (err) {
            console.log(err);
            callback(err);
          } else if (result != null && bcrypt.compareSync(args.password, result.password) ) {
            console.log('Found:', result);
            var token = jwt.sign({ data: result.username }, 'warcry', { expiresIn: '1h' });
            console.log(token);
            var resultJson = {
              success: true,
              message: 'Enjoy your token!',
              data: result,
              token: token
            }
            callback(resultJson);
          } else {
            console.log('User not found!');
            callback('User not found!');
          }
        });
    },
    getMembers: function(callback) {
        Members.find(function (err, result) {
          if (err) {
            console.log(err);
          } else if (result) {
            console.log('Found:', result);
            callback(result)
          } else {
            console.log('User not found!');
          }
        });
    },
    createMember: function(args, callback) {
        if((args.username != undefined && args.password != undefined) && (args.username != "" && args.password != "")) {
            var hash = bcrypt.hashSync(args.password);
            var member = new Members({
                username: args.username,
                password: hash
            });
            member.save(function (err, result) {
              if (err) {
                console.log(err);
              } else {
                callback(result)
              }
            });
        }
        else {
             callback("ข้อมูลไม่ถูกต้อง")
        }
    },
    deleteMember: function(args, callback) {
        varmember_id = args.id;
        Members.remove({_id :member_id}, function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    },
    deleteMembers: function(callback) {
        Members.remove(function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    }
};
