var { Orders } = require('./db.js');

module.exports = {
    getOrder: function(order_name, callback) {
        Orders.findOne({name: order_name}, function (err, result) {
          if (err) {
            console.log(err);
          } else if (result) {
            console.log('Found:', result);
            callback(result)
          } else {
            console.log('User not found!');
          }
        });
    },
    getOrders: function(callback) {
        Orders.find(function (err, result) {
          if (err) {
            console.log(err);
          } else if (result) {
            console.log('Found:', result);
            callback(result)
          } else {
            console.log('User not found!');
          }
        });
    },
    createOrder: function(args, callback) {
        var order_params = args.order;
        var order = new Orders({
            name: order_params.name,
            price: order_params.price,
            amount: order_params.amount,
            category: order_params.category,
            status: order_params.status
        });
        order.save(function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    },
    deleteOrder: function(args, callback) {
        var order_id = args.id;
        Orders.remove({_id : order_id}, function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    },
    updateOrder: function(args, callback) {
        var order_id = args.id;
        var order_name = args.order.name;
        var order_price = args.order.price;
        var order_amount = args.order.amount;
        var order_category = args.order.category;
        var order_status = args.order.status;
        var editOrder = {
          name: order_name,
          price: order_price,
          amount: order_amount,
          category: order_category,
          status: order_status
        }
        Orders.update({_id : order_id}, {$set: editOrder }, function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    },
    updateStatusOrder: function(args, callback) {
        console.log("aaa", args);
        var order_id = args.id;
        var order_status = args.status;
        var editOrder = {
          status: order_status
        }
        Orders.update({_id : order_id}, {$set: editOrder }, function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    }
};
