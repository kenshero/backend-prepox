var graphql = require('graphql');

var GraphQLSchema = graphql.GraphQLSchema;
var GraphQLObjectType = graphql.GraphQLObjectType;

var GraphQLString = graphql.GraphQLString;
var GraphQLNonNull = graphql.GraphQLNonNull;

var { getOrderFields, getOrdersFields, createOrderFields, deleteOrderFields, editOrderFields, subscribeStatusOrder} = require('./orderobjecttype.js');
var { getProductFields, getProductsFields, createProductFields, deleteProductField, editProductField } = require('./productobjecttype.js');

var queryType = new GraphQLObjectType({
  name: "queryOrder",
  description: "orders query",
  fields: () => ({
    order: getOrderFields,
    orders: getOrdersFields,
    product: getProductFields,
    products: getProductsFields
  })
});


var mutationType = new GraphQLObjectType({
    name: 'Mutaion',
    description: 'The root Mutation type',
    fields: {
        createOrder: createOrderFields
    }
})

var subscriptionType = new GraphQLObjectType({
    name: 'Subscription',
    description: 'The root subscription type',
    fields: {
        subscribeStatusOrder: subscribeStatusOrder
    }
})

var UserGraphQLSchema = new GraphQLSchema({
    query: queryType,
    mutation: mutationType,
    subscription: subscriptionType
});

module.exports = UserGraphQLSchema
