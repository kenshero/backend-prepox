var graphql = require('graphql');

var GraphQLSchema = graphql.GraphQLSchema;
var GraphQLObjectType = graphql.GraphQLObjectType;

var { getOrderFields, getOrdersFields, createOrderFields, deleteOrderFields, editOrderFields, updateStatusOrder} = require('./orderobjecttype.js');
var { getProductFields, getProductsFields, createProductFields, deleteProductField, editProductField } = require('./productobjecttype.js');

var queryType = new GraphQLObjectType({
  name: "queryOrder",
  description: "orders query",
  fields: () => ({
    order: getOrderFields,
    orders: getOrdersFields,
    product: getProductFields,
    products: getProductsFields
  })
});


var mutationType = new GraphQLObjectType({
    name: 'Mutaion',
    description: 'The root Mutation type',
    fields: {
        createOrder: createOrderFields,
        deleteOrder: deleteOrderFields,
        editOrder: editOrderFields,
        updateStatusOrder: updateStatusOrder,
        createProductFields: createProductFields,
        deleteProduct: deleteProductField,
        editProduct: editProductField
    }
})

var AdminGraphQLSchema = new GraphQLSchema({
    query: queryType,
    mutation: mutationType
});

module.exports = AdminGraphQLSchema

