var { Products } = require('./db.js');

module.exports = {
    getProduct: function(product_id, callback) {
        Products.findOne({_id: product_id}, function (err, result) {
          if (err) {
            console.log(err);
          } else if (result) {
            console.log('Found:', result);
            callback(result)
          } else {
            console.log('User not found!');
          }
        });
    },
    getProducts: function(callback) {
        Products.find(function (err, result) {
          if (err) {
            console.log(err);
          } else if (result) {
            console.log('Found:', result);
            callback(result)
          } else {
            console.log('User not found!');
          }
        });
    },
    createProduct: function(args, callback) {
        var product_params = args.product;
        var product = new Products({
            name: product_params.name,
            price: product_params.price
        });
        product.save(function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    },
    deleteProduct: function(args, callback) {
        var product_id = args.id;
        Products.remove({_id : product_id}, function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    },
    updateProduct: function(args, callback) {
        var product_id = args.id;
        var product_name = args.product.name;
        var product_price = args.product.price;
        Products.update({_id : product_id}, {$set: {name: product_name, price: product_price} }, function (err, result) {
          if (err) {
            console.log(err);
          } else {
            callback(result)
          }
        });
    }
};
