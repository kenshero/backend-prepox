var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/prepox');
// Connection URL
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("DB Connected");
});

var Orders = mongoose.model('orders', {
    name: String,
    price: Number,
    amount: Number,
    category: Array,
    status: String
});

var Products = mongoose.model('products', {
    name: String,
    price: Number
});

var Members = mongoose.model('members', {
    username: String,
    password: String
});

module.exports = {
    Orders : Orders,
    Products: Products,
    Members: Members
}