var graphql = require('graphql');

var GraphQLSchema = graphql.GraphQLSchema;
var GraphQLObjectType = graphql.GraphQLObjectType;
var GraphQLString = graphql.GraphQLString;
var GraphQLInt = graphql.GraphQLInt;
var GraphQLList = graphql.GraphQLList;
var GraphQLInputObjectType = graphql.GraphQLInputObjectType;
var GraphQLNonNull = graphql.GraphQLNonNull;

var productquerys = require('./productquerys.js');

var productsType = new GraphQLObjectType({
  name: "products",
  description: "Member of The products",
  fields: () => ({
   _id: {
     type: GraphQLString,
     description: "id of the product",
   },
   name: {
     type: GraphQLString,
     description: "Name of the product",
   },
   price: {
     type: GraphQLInt,
     description: "price of product",
   }
 })
});

var productInputType = new GraphQLInputObjectType({
    name: 'product',
    fields: {
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'Title of product'
        },  
        price: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'The price of the product'
        }
    }
});

var getProductFields = {
  type: productsType,
  args: {
    product_id: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  resolve: function(_, args){
    return new Promise((resolve, reject) => {
      productquerys.getProduct( args.product_id , data => resolve(data) )
    })
  }
}

var getProductsFields = {
  type: new GraphQLList(productsType),
  args: {},
  resolve: function(_, args){
    return new Promise((resolve, reject) => {
      productquerys.getProducts( data => resolve(data) )
    })
  }
}

var createProductFields = {
    type: productsType,
    args: {
        product: {
            type: new GraphQLNonNull(productInputType)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        productquerys.createProduct(args, data => resolve(data))
      })
    }
}

var deleteProductField = {
    type: productsType,
    args: {
        id: {
          type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        productquerys.deleteProduct(args, data => resolve(data))
      })
    }
}

var editProductField = {
    type: productsType,
    args: {
        id: {
          type: new GraphQLNonNull(GraphQLString)
        },
        product: {
            type: new GraphQLNonNull(productInputType)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        productquerys.updateProduct(args, data => resolve(data))
      })
    }
}

module.exports = {
  getProductFields: getProductFields,
  getProductsFields: getProductsFields,
  createProductFields: createProductFields,
  deleteProductField: deleteProductField,
  editProductField: editProductField
}