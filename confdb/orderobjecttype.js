var graphql = require('graphql');

var GraphQLSchema = graphql.GraphQLSchema;
var GraphQLObjectType = graphql.GraphQLObjectType;
var GraphQLString = graphql.GraphQLString;
var GraphQLInt = graphql.GraphQLInt;
var GraphQLList = graphql.GraphQLList;
var GraphQLInputObjectType = graphql.GraphQLInputObjectType;
var GraphQLNonNull = graphql.GraphQLNonNull;

var orderquerys = require('./orderquerys.js');

var ordersType = new GraphQLObjectType({
  name: "orders",
  description: "Member of The orders",
  fields: () => ({
   _id: {
     type: GraphQLString,
     description: "id of the order",
   },
   name: {
     type: GraphQLString,
     description: "Name of the order",
   },
   price: {
     type: GraphQLInt,
     description: "price of order",
   },
   amount: {
     type: GraphQLInt,
     description: "amount of order",
   },
   category: {
     type: new GraphQLList(GraphQLString),
     description: "category of order",
   },
   status: {
     type: GraphQLString,
     description: "status of order",
   }
 })
});

var orderEditInputType = new GraphQLInputObjectType({
    name: 'OrderEditInput',
    fields: {
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'name of order'
        },
        price: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'The price of the order'
        },
        amount: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'The amount of the order'
        },
        category: {
            type: new GraphQLNonNull(new GraphQLList(GraphQLString)),
            description: 'The category of the order'
        }
    }
});

var orderCreateInputType = new GraphQLInputObjectType({
    name: 'OrderCreateInput',
    fields: {
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'name of order'
        },
        price: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'The price of the order'
        },
        amount: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'The amount of the order'
        },
        category: {
            type: new GraphQLNonNull(new GraphQLList(GraphQLString)),
            description: 'The category of the order'
        },
        category: {
            type: new GraphQLNonNull(new GraphQLList(GraphQLString)),
            description: 'The category of the order'
        },
        status: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'status of order'
        }
    }
});

var getOrderFields = {
  type: ordersType,
  args: {
    order_name: {
      type: GraphQLString
    }
  },
  resolve: function(_, args){
    return new Promise((resolve, reject) => {
      orderquerys.getOrder(args.order_name, data => resolve(data) )
    })
  }
}

var getOrdersFields = {
  type: new GraphQLList(ordersType),
  args: {},
  resolve: function(_, args){
    return new Promise((resolve, reject) => {
      orderquerys.getOrders( data => resolve(data) )
    })
  }
}

var deleteOrderFields = {
    type: ordersType,
    args: {
        id: {
          type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        orderquerys.deleteOrder(args, data => resolve(data))
      })
    }
}

var createOrderFields = {
    type: ordersType,
    args: {
        order: {
            type: new GraphQLNonNull(orderCreateInputType)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        orderquerys.createOrder(args, data => resolve(data))
      })
    }
}

var editOrderFields = {
    type: ordersType,
    args: {
        id: {
          type: new GraphQLNonNull(GraphQLString)
        },
        order: {
            type: new GraphQLNonNull(orderEditInputType)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        orderquerys.updateOrder(args, data => resolve(data))
      })
    }
}

var updateStatusOrder = {
    type: ordersType,
    args: {
        id: {
          type: new GraphQLNonNull(GraphQLString)
        },
        status: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve: (_, args) => {
      return new Promise((resolve, reject) => {
        orderquerys.updateStatusOrder(args, data => resolve(data))
      })
    }
}

var subscribeStatusOrder = {
    type: ordersType,
    args: {
        id: {
          type: new GraphQLNonNull(GraphQLString)
        },
        status: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve: (_, args) => {
      console.log("args : ", args)
      return new Promise((resolve, reject) => {
      })
    }
}


  // subscription: new GraphQLObjectType({
  //   name: 'Subscription',
  //   fields: {
  //     testSubscription: {
  //       type: GraphQLString,
  //       resolve: function (root) {
  //         return root;
  //       },
  //     },
  //     testContext: {
  //       type: GraphQLString,
  //       resolve(rootValue, args, context) {
  //         return context;
  //       },
  //     },
  //     testFilter: {
  //       type: GraphQLString,
  //       resolve: function (root, { filterBoolean }) {
  //         return filterBoolean ? 'goodFilter' : 'badFilter';
  //       },
  //       args: {
  //         filterBoolean: { type: GraphQLBoolean },
  //       },
  //     },
  //     testFilterMulti: {
  //       type: GraphQLString,
  //       resolve: function (root, { filterBoolean }) {
  //         return filterBoolean ? 'goodFilter' : 'badFilter';
  //       },
  //       args: {
  //         filterBoolean: { type: GraphQLBoolean },
  //         a: { type: GraphQLString },
  //         b: { type: GraphQLInt },
  //       },
  //     },
  //     testChannelOptions: {
  //       type: GraphQLString,
  //       resolve: function (root) {
  //         return root;
  //       },
  //     },
  //   },
  // })

module.exports = {
    getOrderFields: getOrderFields,
    getOrdersFields: getOrdersFields,
    createOrderFields: createOrderFields,
    deleteOrderFields: deleteOrderFields,
    editOrderFields: editOrderFields,
    updateStatusOrder: updateStatusOrder,
    subscribeStatusOrder: subscribeStatusOrder,
}