var { createServer } = require('http');
var express = require('express');
var graphqlHTTP = require('express-graphql');

var AdminGraphQLSchema = require('./confdb/schemaAdmin.js');
var UserGraphQLSchema = require('./confdb/schemaUser.js');
var { PubSub, SubscriptionManager } = require('graphql-subscriptions');
var { SubscriptionServer } = require('subscriptions-transport-ws');

var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');

app.use(cors())
const pubsub = new PubSub();

const subscriptionManager = new SubscriptionManager({
  UserGraphQLSchema,
  pubsub,
  setupFunctions: {
    subscribeStatusOrder: (options, args) => ({
      newCommentsChannel: {
        filter: comment => comment.repository_name === args.repoFullName,
      },
  }),
  },
});

app.use('/graphqlUser', graphqlHTTP({
  schema: UserGraphQLSchema,
  graphiql: true
}));

require('./routes/routes.js')(app, bodyParser);

app.use('/graphqlAdmin', graphqlHTTP({
  schema: AdminGraphQLSchema,
  graphiql: true
}));

app.use("/", express.static("public"));

var port = process.env.PORT || 3000;
const WS_PORT = process.env.WS_PORT || 3333;
app.listen(port);

// WebSocket server for subscriptions
const websocketServer = createServer((request, response) => {
  response.writeHead(404);
  response.end();
});

websocketServer.listen(WS_PORT, () => console.log( // eslint-disable-line no-console
  `Websocket Server is now running on http://localhost:${WS_PORT}`
));

const server = new SubscriptionServer({ subscriptionManager }, websocketServer);
console.log("Grapql Server running");


pubsub.publish('newCommentsChannel', {
  id: 123,
  content: 'Test',
  repoFullName: 'apollostack/GitHunt-API',
  posted_by: 'helfer',
});