var jwt = require('jsonwebtoken');

function verifyJwt (req, res, next) {
  var token = req.headers.authorization
  if (token) {
    jwt.verify(token, 'warcry', function(err, decoded) {
      if (err) {
        return res.status(401).send({ success: false, message: 'Failed to Unauthorized token.' });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
  }
}

module.exports = function(app, bodyParser) {
    require('./auth')(app, bodyParser)
    app.use((req, res, next) => verifyJwt(req, res, next))
    require('./members')(app, bodyParser)
};

